
COMMERCE CECA MODULE FOR DRUPAL 7.x
-----------------------------------

CONTENTS OF THIS README
-----------------------

   * Description
   * Requirements
   * Installation
   * Support
   * Credits


DESCRIPTION
-----------

Commerce payment method for Spanish CECA payment service.
Official website for this service is http://www.ceca.es/es/index.html


REQUIREMENTS
------------

* Commerce.
* Commerce Order.
* Commerce Payment.

You will need, as usual, Commerce Payment UI just for configuring the payment
method.


INSTALLATION
------------

* Install Commerce CECA module.
* Enable the Rule for Sermepa via Store > Configuration > Payment settings and
  edit the rule settings to set the information provided by your bank office.


SUPPORT
-------

Donation is possible by contacting me via grisendo@gmail.com


CREDITS
-------

7.x-1.x Developed and maintained by grisendo
